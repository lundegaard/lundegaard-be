package com.example.lundegaardexample.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactTypeDto {

    @Schema(description = "Id of Contact type", example = "1", required = true)
    private Integer id;

    @Schema(description = "Name of contact type", example = "Damage", required = true)
    private String name;
}
