package com.example.lundegaardexample.model.dto.request;

import com.example.lundegaardexample.controller.annotations.PolicyNumberValidation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactFormRequestDto {

    @Schema(description = "Subject text", example = "Nefunkční tiskárna", required = true)
    @NotBlank(message = "Subject name must be provided.")
    @Size(min = 1, max = 125, message = "Event name must have max. of 125 chars.")
    private String subject;

    @Schema(description = "Surname of person", example = "Lukáš", required = true)
    @NotBlank(message = "Surname must be provided.")
    @Size(min = 1, max = 125, message = "Surname name must have max. of 125 chars.")
    private String surname;

    @Schema(description = "Surname of person", example = "ABCD12", required = true)
    @NotBlank(message = "Surname must be provided.")
    @PolicyNumberValidation
    @Size(min = 1, max = 125, message = "Surname name must have max. of 125 chars.")
    private String policyNumber;

    @Schema(description = "Name of person", example = "Lukáš", required = true)
    @NotBlank(message = "Name must be provided.")
    @Size(min = 1, max = 125, message = "Name name must have max. of 125 chars.")
    private String name;

    @Schema(description = "Text of contact request", example = "I cannot print on my wireless printer. Please help me!", required = true)
    @NotBlank(message = "Text must be provided. Please write something interest.")
    @Size(min = 1, max = 500, message = "Text must have max. of 125 chars.")
    private String text;

    @Schema(description = "Id of contact type request", required = true)
    @NotNull(message = "Contact type must be provided")
    private Integer type;
}
