package com.example.lundegaardexample.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactDto {

    @Schema(description = "ID of the contact", example = "1", required = true)
    private Integer id;

    @Schema(description = "Subject text", example = "Pomoc s PC", required = true)
    private String subject;

    @Schema(description = "Name text", example = "Lukas", required = true)
    private String name;

    @Schema(description = "Surname", example = "Kopecky", required = true)
    private String surname;

    @Schema(description = "Policy number", example = "AABBCC12", required = true)
    private String policyNumber;

    @Schema(description = "Text of contact problem", example = "Problem s tiskarnou a PC.", required = true)
    private String text;

    @Schema(description = "Problem type", required = true)
    private ContactTypeDto type;
}
