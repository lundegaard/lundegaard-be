package com.example.lundegaardexample.controller.impl;

import com.example.lundegaardexample.controller.ContactTypeController;
import com.example.lundegaardexample.model.dto.ContactTypeDto;
import com.example.lundegaardexample.model.dto.request.ContactTypeRequestDto;
import com.example.lundegaardexample.service.ContactTypeService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@Validated
public class ContactTypeControllerImpl implements ContactTypeController {

    private final ContactTypeService contactTypeService;

    @Override
    public List<ContactTypeDto> findAllContactTypes() {
        return contactTypeService.findAllContactTypes();
    }

    @Override
    public ContactTypeDto createContactType(ContactTypeRequestDto contactTypeRequestDto) {
        return contactTypeService.createContactType(contactTypeRequestDto);
    }
}
