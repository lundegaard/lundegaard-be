package com.example.lundegaardexample.controller.impl;

import com.example.lundegaardexample.controller.ContactController;
import com.example.lundegaardexample.model.dto.ContactDto;
import com.example.lundegaardexample.model.dto.request.ContactFormRequestDto;
import com.example.lundegaardexample.service.ContactService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
@Validated
public class ContactControllerImpl implements ContactController {

    private final ContactService contactService;

    @Override
    public ContactDto handleNewContact(ContactFormRequestDto contactFormRequestDto) {
        return contactService.handleNewContactEntity(contactFormRequestDto);
    }
}
