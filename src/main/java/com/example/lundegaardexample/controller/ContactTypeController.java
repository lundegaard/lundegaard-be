package com.example.lundegaardexample.controller;

import com.example.lundegaardexample.model.dto.ContactTypeDto;
import com.example.lundegaardexample.model.dto.request.ContactTypeRequestDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(Routes.CONTACT_TYPE_FORM)
@Tag(name = "Contact Type Form", description = "Use endpoint to create new contact type")
public interface ContactTypeController {

    @GetMapping
    List<ContactTypeDto> findAllContactTypes();

    @PostMapping
    ContactTypeDto createContactType(@Valid @RequestBody ContactTypeRequestDto contactTypeRequestDto);
}
