package com.example.lundegaardexample.controller.annotations;

import com.example.lundegaardexample.controller.annotations.impl.PolicyNumberValidationImpl;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PolicyNumberValidationImpl.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PolicyNumberValidation {
    String message() default "Invalid policy number, type only AlphaNumeric characters";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
