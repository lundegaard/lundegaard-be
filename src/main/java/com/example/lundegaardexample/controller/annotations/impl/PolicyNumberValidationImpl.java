package com.example.lundegaardexample.controller.annotations.impl;

import com.example.lundegaardexample.controller.annotations.PolicyNumberValidation;
import com.example.lundegaardexample.util.Regex;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PolicyNumberValidationImpl implements ConstraintValidator<PolicyNumberValidation, String> {

    @Override
    public void initialize(PolicyNumberValidation contactNumber) {
    }

    @Override
    public boolean isValid(String contactField,
                           ConstraintValidatorContext cxt) {
        return contactField != null && contactField.matches(Regex.ALPHA_NUMERIC_REGEX_VALIDATION);
    }

}
