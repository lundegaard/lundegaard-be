package com.example.lundegaardexample.controller;

public class Routes {

    public static final String API = "/api";
    public static final String V1_VERSION =  API + "/v1";
    public static final String CONTACT_FORM = V1_VERSION + "/contact-form";

    public static final String CONTACT_TYPE_FORM = V1_VERSION + "/contact-type";
}
