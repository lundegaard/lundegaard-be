package com.example.lundegaardexample.controller;

import com.example.lundegaardexample.model.dto.ContactDto;
import com.example.lundegaardexample.model.dto.request.ContactFormRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping(Routes.CONTACT_FORM)
@Tag(name = "Contact Form", description = "Use endpoint to create new contact form entity")
public interface ContactController {

    @PostMapping
    @Operation(summary = "Create new entity", description = "Create the new entity and react in contact form.")
    ContactDto handleNewContact(@Valid @RequestBody ContactFormRequestDto contactFormRequestDto);

}
