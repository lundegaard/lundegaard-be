package com.example.lundegaardexample.controller;

import com.example.lundegaardexample.error.model.Rfc7807Error;
import com.example.lundegaardexample.error.model.ValidationException;
import com.example.lundegaardexample.model.ContactType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.Objects;

@ControllerAdvice
public class GlobalExceptionHandler {



	@ExceptionHandler({MethodArgumentNotValidException.class})
	public final ResponseEntity<?> validationArgumentException(MethodArgumentNotValidException ex) {
		Rfc7807Error error = new Rfc7807Error()
				.withTitle("Validation error on parameter: " + Objects.requireNonNull(ex.getBindingResult().getFieldError()).getField())
				.withDetail("Validation error")
				.withCode(HttpStatus.BAD_REQUEST);
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		fieldErrors.forEach(e ->
			error.addParam(
					new Rfc7807Error.
							InvalidParam(
							e.getField(), e.getDefaultMessage())
			)
		);


		return ResponseEntity.status(400)
				.body(error);
	}

	@ExceptionHandler({ValidationException.class})
	public final ResponseEntity<?> validationException(ValidationException ex, WebRequest request) {
		return ResponseEntity.status(400)
			.body(ex.getRfc7807Error());
	}

	@ExceptionHandler({Exception.class})
	public final ResponseEntity<?> handleGeneralException(Exception ex, WebRequest request) {
		ex.printStackTrace();
		return ResponseEntity.status(400)
			.body(Rfc7807Error.parse(ex));
	}

}
