package com.example.lundegaardexample.mapper;

import com.example.lundegaardexample.mapper.decorators.ContactDecorator;
import com.example.lundegaardexample.model.Contact;
import com.example.lundegaardexample.model.dto.ContactDto;
import com.example.lundegaardexample.model.dto.request.ContactFormRequestDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { ContactTypeMapper.class})
@DecoratedWith(ContactDecorator.class)
public interface ContactMapper extends EditingMapper<Contact> {

    @Mapping(target = "type", ignore = true)
    Contact entityFromRequestDto(ContactFormRequestDto requestDto);

    ContactDto fromEntityToDto(Contact contact);
}
