package com.example.lundegaardexample.mapper.editor;

import com.example.lundegaardexample.model.ContactType;
import org.springframework.stereotype.Component;

@Component
public class ContactTypeEditor implements Editor<ContactType> {
    @Override
    public void edit(ContactType toEdit, ContactType editBy) {
        // TODO impl method on update entity
    }
}
