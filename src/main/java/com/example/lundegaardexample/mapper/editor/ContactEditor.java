package com.example.lundegaardexample.mapper.editor;

import com.example.lundegaardexample.model.Contact;
import org.springframework.stereotype.Component;

@Component
public class ContactEditor implements Editor<Contact> {
    @Override
    public void edit(Contact toEdit, Contact editBy) {
        // TODO add method if update entity
    }
}
