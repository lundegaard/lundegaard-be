package com.example.lundegaardexample.mapper.editor;

public interface Editor<T> {

	void edit(T toEdit, T editBy);

}
