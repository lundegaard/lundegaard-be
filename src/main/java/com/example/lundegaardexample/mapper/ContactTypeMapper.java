package com.example.lundegaardexample.mapper;

import com.example.lundegaardexample.mapper.decorators.ContactTypeDecorator;
import com.example.lundegaardexample.model.ContactType;
import com.example.lundegaardexample.model.dto.ContactTypeDto;
import com.example.lundegaardexample.model.dto.request.ContactTypeRequestDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = { ContactTypeMapper.class })
@DecoratedWith(ContactTypeDecorator.class)
public interface ContactTypeMapper {

    ContactType entityFromRequestDto(ContactTypeRequestDto requestDto);

    ContactTypeDto entityToDto(ContactType entity);

    List<ContactTypeDto> entitiesToDtoList(List<ContactType> entityList);
}
