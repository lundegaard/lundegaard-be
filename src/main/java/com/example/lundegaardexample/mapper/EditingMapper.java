package com.example.lundegaardexample.mapper;

public interface EditingMapper<T> {

	default void edit(T toEdit, T editBy) {
		//uses decorator implementation for Mapstruct mapping
	}

}
