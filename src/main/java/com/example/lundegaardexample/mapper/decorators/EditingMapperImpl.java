package com.example.lundegaardexample.mapper.decorators;

import com.example.lundegaardexample.mapper.EditingMapper;
import com.example.lundegaardexample.mapper.editor.Editor;
import org.springframework.beans.factory.annotation.Autowired;

public class EditingMapperImpl<T> implements EditingMapper<T> {

	private Editor<T> editor;

	public void edit(T toEdit, T editBy) {
		editor.edit(toEdit, editBy);
	}

	@Autowired
	public void setOwnerEditor(Editor<T> editor) {
		this.editor = editor;
	}

}
