package com.example.lundegaardexample.mapper.decorators;

import com.example.lundegaardexample.error.model.Rfc7807Error;
import com.example.lundegaardexample.error.model.ValidationException;
import com.example.lundegaardexample.mapper.ContactMapper;
import com.example.lundegaardexample.model.Contact;
import com.example.lundegaardexample.model.ContactType;
import com.example.lundegaardexample.model.dto.request.ContactFormRequestDto;
import com.example.lundegaardexample.repository.ContactTypeRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.time.LocalDateTime;
import java.util.List;

public abstract class ContactDecorator extends EditingMapperImpl<Contact> implements ContactMapper {

    @Autowired
    private ContactMapper delegate;

    @Autowired
    private ContactTypeRepository contactTypeRepository;

    @SneakyThrows
    @Override
    public Contact entityFromRequestDto(ContactFormRequestDto requestDto) {
        ContactType typeEntity = contactTypeRepository.getById(requestDto.getType());
        if (typeEntity == null) {
            Rfc7807Error error = new Rfc7807Error()
                    .withTitle("Contact type has not been found")
                    .withDetail("Validation error")
                    .withCode(HttpStatus.BAD_REQUEST)
                    .withInstance(ContactType.class.getName());
            error.addParam(
                    new Rfc7807Error.
                            InvalidParam(
                            "Contact type",
                            "Contact type with id " + requestDto.getType() + " does not exists")
            );

            throw new ValidationException(error);
        }
        Contact contact = delegate.entityFromRequestDto(requestDto);
        contact.setCreatedAt(LocalDateTime.now());
        contact.setType(typeEntity);
        return contact;
    }
}
