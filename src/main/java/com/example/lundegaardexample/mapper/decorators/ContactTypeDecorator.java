package com.example.lundegaardexample.mapper.decorators;

import com.example.lundegaardexample.mapper.ContactTypeMapper;
import com.example.lundegaardexample.model.ContactType;
import com.example.lundegaardexample.model.dto.request.ContactTypeRequestDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

public abstract class ContactTypeDecorator extends EditingMapperImpl<ContactType> implements ContactTypeMapper {

    @Autowired
    private ContactTypeMapper delegate;

    @Override
    public ContactType entityFromRequestDto(ContactTypeRequestDto requestDto) {
        ContactType contactType = delegate.entityFromRequestDto(requestDto);
        contactType.setCreatedAt(LocalDateTime.now());
        return contactType;
    }
}
