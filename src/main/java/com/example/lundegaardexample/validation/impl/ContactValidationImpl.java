package com.example.lundegaardexample.validation.impl;

import com.example.lundegaardexample.model.Contact;
import com.example.lundegaardexample.repository.ContactRepository;
import com.example.lundegaardexample.validation.Validator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ContactValidationImpl implements Validator<Contact> {

    private final ContactRepository contactRepository;

    @Override
    public void validateBeforeCreation(Contact object) {

    }

    @Override
    public void validateBeforeEditing(Contact object) {

    }
}
