package com.example.lundegaardexample.validation.impl;

import com.example.lundegaardexample.model.ContactType;
import com.example.lundegaardexample.repository.ContactTypeRepository;
import com.example.lundegaardexample.validation.Validator;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ContactTypeValidationImpl implements Validator<ContactType> {

    private final ContactTypeRepository contactTypeRepository;

    @SneakyThrows
    @Override
    public void validateBeforeCreation(ContactType entity) {
    }

    @Override
    public void validateBeforeEditing(ContactType entity) {

    }
}
