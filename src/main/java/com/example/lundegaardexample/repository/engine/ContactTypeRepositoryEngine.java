package com.example.lundegaardexample.repository.engine;

import com.example.lundegaardexample.model.ContactType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactTypeRepositoryEngine extends CrudRepository<ContactType, Integer> {

}
