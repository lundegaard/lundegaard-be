package com.example.lundegaardexample.repository.engine;


import com.example.lundegaardexample.model.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepositoryEngine extends CrudRepository<Contact, Integer> {

}
