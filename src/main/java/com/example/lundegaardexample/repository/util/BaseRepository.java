package com.example.lundegaardexample.repository.util;

import java.util.List;

public interface BaseRepository<T> {

    public List<T> findAll();

    public T getById(int id);

    public T getNotNull(int id);

    public T save(T object);

    public void deleteById(int id);

    public void delete(T object);

    public void clearCache();

}
