package com.example.lundegaardexample.repository;

import com.example.lundegaardexample.model.ContactType;
import com.example.lundegaardexample.repository.util.BaseRepository;

public interface ContactTypeRepository extends BaseRepository<ContactType> {

}
