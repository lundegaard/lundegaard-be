package com.example.lundegaardexample.repository;

import com.example.lundegaardexample.model.Contact;
import com.example.lundegaardexample.repository.util.BaseRepository;

public interface ContactRepository extends BaseRepository<Contact> {
}
