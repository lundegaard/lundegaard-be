package com.example.lundegaardexample.repository.impl;

import com.example.lundegaardexample.model.ContactType;
import com.example.lundegaardexample.repository.ContactTypeRepository;
import com.example.lundegaardexample.repository.engine.ContactTypeRepositoryEngine;
import com.example.lundegaardexample.repository.util.AbstractRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@AllArgsConstructor
public class ContactTypeRepositoryImpl extends AbstractRepository<ContactType> implements ContactTypeRepository {

    private static final String CACHE_NAME = "CONTACT_TYPES";

    private final ContactTypeRepositoryEngine contactTypeRepositoryEngine;

    @Cacheable(cacheNames = CACHE_NAME)
    @Override
    public List<ContactType> findAll() {
        return super.findAll();
    }

    @Override
    @Transactional
    @CacheEvict(value=CACHE_NAME, allEntries=true)
    public ContactType save(ContactType toSave) {
        return super.save(toSave);
    }

    @Override
    @Transactional
    @CacheEvict(value=CACHE_NAME, allEntries=true)
    public void delete(ContactType object) {
        super.delete(object);
    }

    @Override
    @Transactional
    @CacheEvict(value=CACHE_NAME, allEntries=true)
    public void deleteById(int id) {
        super.deleteById(id);
    }



    @Override
    protected CrudRepository<ContactType, Integer> getRepositoryEngine() {
        return contactTypeRepositoryEngine;
    }

}
