package com.example.lundegaardexample.repository.impl;

import com.example.lundegaardexample.model.Contact;
import com.example.lundegaardexample.repository.util.AbstractRepository;
import com.example.lundegaardexample.repository.ContactRepository;
import com.example.lundegaardexample.repository.engine.ContactRepositoryEngine;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ContactRepositoryImpl extends AbstractRepository<Contact> implements ContactRepository {

    private final ContactRepositoryEngine contactRepositoryEngine;

    @Override
    protected CrudRepository<Contact, Integer> getRepositoryEngine() {
        return contactRepositoryEngine;
    }
}
