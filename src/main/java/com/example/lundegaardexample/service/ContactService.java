package com.example.lundegaardexample.service;

import com.example.lundegaardexample.model.dto.ContactDto;
import com.example.lundegaardexample.model.dto.request.ContactFormRequestDto;

public interface ContactService {

    ContactDto handleNewContactEntity(ContactFormRequestDto contactFormRequestDto);
}
