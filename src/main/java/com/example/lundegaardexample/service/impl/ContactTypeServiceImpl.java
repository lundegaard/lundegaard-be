package com.example.lundegaardexample.service.impl;

import com.example.lundegaardexample.mapper.ContactTypeMapper;
import com.example.lundegaardexample.model.ContactType;
import com.example.lundegaardexample.model.dto.ContactTypeDto;
import com.example.lundegaardexample.model.dto.request.ContactTypeRequestDto;
import com.example.lundegaardexample.repository.ContactTypeRepository;
import com.example.lundegaardexample.service.ContactTypeService;
import com.example.lundegaardexample.validation.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ContactTypeServiceImpl implements ContactTypeService {

    private final ContactTypeRepository contactTypeRepository;

    private final ContactTypeMapper contactTypeMapper;

    private final Validator<ContactType> validator;

    @Override
    public List<ContactTypeDto> findAllContactTypes() {
        List<ContactType> typeEntities = contactTypeRepository.findAll();
        return contactTypeMapper.entitiesToDtoList(typeEntities);
    }

    @Override
    public ContactTypeDto createContactType(ContactTypeRequestDto contactTypeRequestDto) {
        ContactType newContactType = contactTypeMapper.entityFromRequestDto(contactTypeRequestDto);
        validator.validateBeforeCreation(newContactType);
        ContactType contactType = contactTypeRepository.save(newContactType);
        return contactTypeMapper.entityToDto(contactType);
    }
}
