package com.example.lundegaardexample.service.impl;

import com.example.lundegaardexample.mapper.ContactMapper;
import com.example.lundegaardexample.model.Contact;
import com.example.lundegaardexample.model.dto.ContactDto;
import com.example.lundegaardexample.model.dto.request.ContactFormRequestDto;
import com.example.lundegaardexample.repository.ContactRepository;
import com.example.lundegaardexample.service.ContactService;
import com.example.lundegaardexample.validation.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class ContactServiceImpl implements ContactService {

    private final ContactRepository contactRepository;
    private final ContactMapper contactMapper;
    private final Validator<Contact> validator;

    @Override
    public ContactDto handleNewContactEntity(ContactFormRequestDto contactFormRequestDto) {
        Contact newContact = contactMapper.entityFromRequestDto(contactFormRequestDto);
        validator.validateBeforeCreation(newContact);
        Contact contact = contactRepository.save(newContact);
        return contactMapper.fromEntityToDto(contact);
    }
}
