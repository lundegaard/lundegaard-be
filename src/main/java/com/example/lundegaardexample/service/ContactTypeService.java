package com.example.lundegaardexample.service;

import com.example.lundegaardexample.model.dto.ContactTypeDto;
import com.example.lundegaardexample.model.dto.request.ContactTypeRequestDto;

import java.util.List;

public interface ContactTypeService {

    List<ContactTypeDto> findAllContactTypes();

    ContactTypeDto createContactType(ContactTypeRequestDto contactTypeRequestDto);
}
