package com.example.lundegaardexample.error.model;


import lombok.Data;
import lombok.experimental.UtilityClass;

@UtilityClass
@Data
public class ValidationErrorsConstants {

	public String FIELD_REQUIRED = "FIELD_REQUIRED";

}
