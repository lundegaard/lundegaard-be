package com.example.lundegaardexample.error.model;

public class UserPropagableException extends IllegalArgumentException {

	public UserPropagableException() {
	}

	public UserPropagableException(String s) {
		super(s);
	}

}
