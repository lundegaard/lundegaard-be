package com.example.lundegaardexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class LundegaardExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(LundegaardExampleApplication.class, args);
    }

}
