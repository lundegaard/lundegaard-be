CREATE TABLE "public"."contact_type"
(
    id         serial PRIMARY KEY,
    name       varchar(125) NOT NULL CHECK (name <> ''),
    created_at timestamp
);

CREATE TABLE "public"."contact"
(
    id            serial PRIMARY KEY,
    surname       varchar(125) NOT NULL,
    name          varchar(125) NOT NULL,
    subject       varchar(125) NOT NULL,
    policy_number varchar(125) NOT NULL,
    created_at    timestamp,
    text          varchar(1000) NOT NULL,
    type_id       integer      NOT NULL
);

ALTER TABLE "public"."contact"
    ADD CONSTRAINT "policy_validation_alphanumeric" CHECK (contact.policy_number ~ $$^[a-zA-Z0-9_]$$),
    ADD CONSTRAINT "policy_validation_not_blank" CHECK (contact.policy_number <> ''),
    ADD CONSTRAINT "surname_not_blank" CHECK (contact.surname <> ''),
    ADD CONSTRAINT "subject_not_blank" CHECK (contact.subject <> ''),
    ADD CONSTRAINT "text_not_blank" CHECK (contact.text <> ''),
    ADD CONSTRAINT "name_not_blank" CHECK (contact.name <> '');

ALTER TABLE "public"."contact"
    ADD FOREIGN KEY ("type_id") REFERENCES "public"."contact_type" ("id") ON DELETE SET NULL;